# FACTORY METHOD PATTERN #

Factory method pattern is a pattern that uses for create an object which uses factory methods to create objects without exposing the creation logic and specifying the exact class of object that will be created.

### Implementation ###
We're going to create a AppleDevices interface and concrete classes implementing the AppleDevices interface. A factory class AppleFactory is defined as a next step.

FactoryPatternDemo, our demo class will use AppleFactory to get a AppleDevices object. It will pass information of iPhone, Macbook and Watch to AppleFactory to get the type of object it needs.
#### Step 1 : ####
Create an interface.  
*AppleDevices.java*

```
public interface AppleDevices {
	public void construct();
}
```
  
#### Step 2 : ####
Create concrete classes implementing the same interface that is AppleDivices.  
*IPhone.java*
```
public class IPhone implements AppleDevices {
	public void construct() {
		System.out.println("Building iPhone");
	}
}
```
*Macbook.java*
```
public class Macbook implements AppleDevices {
	public void construct() {
		System.out.println("Buliding Macbook");
	}
}
```
*Watch.java*
```
public class Watch implements AppleDevices {
	public void construct() {
		System.out.println("Building watch");
	}
}
```
#### Step 3 : ####
Create a Factory to generate object of concrete class based on given information.  
*AppleFactory.java*
```
public class AppleFactory {
	public AppleDevices getDevice( String name ) {
		if( name == null ) {
	         return null;
	    }
		else if ( name.equalsIgnoreCase("IPHONE") ) {
			return new IPhone(); 
		}
		else if ( name.equalsIgnoreCase("MACBOOK") ) {
			return new Macbook();
		}
		else if ( name.equalsIgnoreCase("WATCH") ) {
			return new Watch();
		}
		return null;
	}
}
```
#### Step 4 : ####
Use the Factory to get object of concrete class by passing an information such as type. In this example, we create FactoryPatternDemo class to execute the program.  
*FactoryPatternDemo.java*
```
public class FactoryPatternDemo {
	public static void main( String [] args) {
		AppleFactory factory = new AppleFactory();
		AppleDevices device1 = factory.getDevice("IPHONE");
		device1.construct();
		AppleDevices device2 = factory.getDevice("Macbook");
		device2.construct();
		AppleDevices device3 = factory.getDevice("watch");
		device3.construct();
	}
}
```
#### Step 5 : ####
Verify the output. We will see the output like this.
```
Building iPhone
Buliding Macbook
Building watch
```
From the above results, we will see the different outputs of each line, even though it call the same method and have the same data type. Each device initialize the value by using the factory method with different name. We don't have to know the data type of the new object from factory. But all the objects are under the AppleDevices. This is the polymorphism.  

#### This is the [example code](https://bitbucket.org/supanat_p/factory-method/src/dafc55f8674a2ded275bcfc090df7add878be7ac/src/?at=master) and the [exercise](https://bytebucket.org/supanat_p/factory-method/raw/dafc55f8674a2ded275bcfc090df7add878be7ac/Factory%20Method%20Pattern%20Practice.pdf) ####  

#### References ####
http://www.tutorialspoint.com/design_pattern/factory_pattern.htm  
http://howtodoinjava.com/2012/10/23/implementing-factory-design-pattern-in-java/  
http://en.wikipedia.org/wiki/Factory_method_pattern