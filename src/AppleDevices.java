/**
 * This source code is Copyright 2015 by Supanat Pokturng and Tuangrat Mungmeerattanaworachot
 * Interface of Apple's devices.
 * @author Supanat Pokturng, Tuangrat Mungmeerattanaworachot
 * @version 2015.04.27
 */
public interface AppleDevices {
	/* Construct this device */
	public void construct();
}
