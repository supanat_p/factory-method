/**
 * This source code is Copyright 2015 by Supanat Pokturng and Tuangrat Mungmeerattanaworachot.
 * Execute the program.
 * @author Supanat Pokturng and Tuangrat Mungmeerattanaworachot
 * @version 2015.04.27
 */
public class FactoryPatternDemo {
	
	/**
	 * Execute the program.
	 * @param args is an arguments
	 */
	public static void main( String [] args) {
		AppleFactory factory = new AppleFactory();
		AppleDevices device1 = factory.getDevice("IPHONE");
		device1.construct();
		AppleDevices device2 = factory.getDevice("Macbook");
		device2.construct();
		AppleDevices device3 = factory.getDevice("watch");
		device3.construct();
	}
}