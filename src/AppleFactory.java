/**
 * This source code is Copyright 2015 by Supanat Pokturng and Tuangrat Mungmeerattanaworachot
 * Factory class of Apple.
 * has factory method to get the device by using the name.
 * @author Supanat Pokturng, Tuangrat Mungmeerattanaworachot
 * @version 2015.04.27
 */
public class AppleFactory {
	
	/**
	 * Get the Apple's device by using name of device.
	 * @param name is String of name of device that gotten.
	 * @return AppleDevices of the device that gotten.
	 */
	public AppleDevices getDevice( String name ) {
		
		if( name == null ) {
	         return null;
	    }
		
		else if ( name.equalsIgnoreCase("IPHONE") ) {
			return new IPhone(); 
		}
		
		else if ( name.equalsIgnoreCase("MACBOOK") ) {
			return new Macbook();
		}
		
		else if ( name.equalsIgnoreCase("WATCH") ) {
			return new Watch();
		}
		
		return null;
	}
}
