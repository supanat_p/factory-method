/**
 * This source code is Copyright 2015 by Supanat Pokturng and Tuangrat Mungmeerattanaworachot.
 * Watch class that is Apple's device.
 * @author Supanat Pokturng and Tuangrat Mungmeerattanaworachot
 * @version 2015.04.27
 */
public class Watch implements AppleDevices {
	
	/**
	 * Construct the Watch.
	 */
	public void construct() {
		System.out.println("Building watch");
	}
}
