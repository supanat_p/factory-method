/**
 * This source code is Copyright 2015 by Supanat Pokturng and Tuangrat Mungmeerattanaworachot.
 * Macbook class that is Apple's device.
 * @author Supanat Pokturng and Tuangrat Mungmeerattanaworachot
 * @version 2015.04.27
 */
public class Macbook implements AppleDevices {
	
	/**
	 * Construct the Macbook.
	 */
	public void construct() {
		System.out.println("Buliding Macbook");
	}
}
